import math
import numpy as np

def quaternion_to_euler(quaternion):
    w,x,y,z = quaternion[0], quaternion[1], quaternion[2], quaternion[3]
    #ROLL
    sinr_cosp = 2 * (w * x + y * z)
    cosr_cosp = 1 - 2 * (x**2 + y**2)
    roll = math.atan2(sinr_cosp, cosr_cosp)

    #PITCH
    sinp = 2 * (w * y - z * x)
    if (abs(sinp) >= 1):
        pitch = math.copysign(math.pi/2, sinp) #use 90 degrees if out of range
    else:
        pitch = math.asin(sinp)

    #YAW
    siny_cosp = 2 * (w * z + x * y)
    cosy_cosp = 1 - 2 * (y**2 + z**2)
    yaw = math.atan2(siny_cosp, cosy_cosp)

    return [roll, pitch, yaw]

def euler_to_quaternion(euler):
    (roll, pitch, yaw) = (euler[0], euler[1], euler[2])
    qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
    qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
    qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
    qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
    return [qw, qx, qy, qz]
