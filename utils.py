import csv
import glob
import numpy as np
import h5py
import os
import pandas as pd

def read_imu(path):
    print(path)
    imu = pd.read_csv(path, usecols=['#timestamp', 'v_RS_R_x', 'v_RS_R_y',
     'v_RS_R_z', 'b_w_RS_S_x', 'b_w_RS_S_y', 'b_w_RS_S_z', 'b_a_RS_S_x',
     'b_a_RS_S_y', 'b_a_RS_S_z'])
    imu = imu.rename(columns={
        '#timestamp': 'timestamp',
        'v_RS_R_x': 'vx',
        'v_RS_R_y': 'vy',
        'v_RS_R_z': 'vz',
        'b_w_RS_S_x': 'wx',
        'b_w_RS_S_y': 'wy',
        'b_w_RS_S_z': 'wz',
        'b_a_RS_S_x': 'ax',
        'b_a_RS_S_y': 'ay',
        'b_a_RS_S_z': 'az'
    })
    return imu

def integrate_imu(df, ts1, ts2):

    date1 = pd.to_datetime(int(ts1))
    date2 = pd.to_datetime(int(ts2))
    mask = (pd.to_datetime(df['timestamp']) >= date1) & (pd.to_datetime(df['timestamp']) <= date2)
    return df[mask].sum(axis=0)


def read_csv(path):
    csv_arr = []
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=' ')
        line_count = 0
        for row in csv_reader:
            #print(row)
            timestamp = [row[0]]
            pose = [float(x) for x in row[1:]]
            csv_arr.append(timestamp+pose)
    return csv_arr

def loadh5(dump_file_full_name):
    ''' Loads a h5 file as dictionary '''
    if(os.path.exists(dump_file_full_name)):
        with h5py.File(dump_file_full_name, 'r') as h5file:
            dict_from_file = readh5(h5file)
        return dict_from_file
    print("File ",dump_file_full_name, " not found")
    return None

def readh5(h5node):
    ''' Recursive function to read h5 nodes as dictionary '''

    dict_from_file = {}
    for _key in h5node.keys():
        if isinstance(h5node[_key], h5py._hl.group.Group):
            dict_from_file[_key] = readh5(h5node[_key])
        else:
            dict_from_file[_key] = h5node[_key][()]
    return dict_from_file

def min_max_norm(v):
    xmin = min(v)
    xmax = max(v)
    return [(x - xmin)/(xmax-xmin) for x in v]
