from keras.layers import Input, concatenate, LSTM, Dense, Activation
from keras.layers import BatchNormalization, TimeDistributed, Dropout
from keras.models import Sequential, Model

def mlp_stacked_lstm(descs_input_shape, kpts_input_shape):
    batch_size = descs_input_shape[0]
    input_descs = Input(batch_shape=(descs_input_shape), name="descriptors")
    mlp_descs = Dense(4096)(input_descs)
    #mlp_descs = TimeDistributed(Dense(25600, activation='tanh'))(input_descs)
    #drop1 = Dropout(0.5)(mlp_descs)
    mlp_descs2 = Dense(2048)(mlp_descs)
    #mlp_descs2 = TimeDistributed(Dense(2560, activation='tanh'))(mlp_descs)
    #drop2 = Dropout(0.5)(mlp_descs2)
    #mlp_descs = Dense(2048)(input_descs)

    input_kpts = Input(batch_shape=(kpts_input_shape), name="kpts")
    #mlp_kpts = TimeDistributed(Dense(2600, activation='tanh'))(input_kpts)
    mlp_kpts = Dense(2600)(input_kpts)
    #drop3 = Dropout(0.5)(mlp_kpts)
    mlp_kpts2 = Dense(260)(mlp_kpts)
    #mlp_kpts2 = TimeDistributed(Dense(260, activation='tanh'))(mlp_kpts)
    #drop4 = Dropout(0.5)(mlp_kpts2)

    mlp_concat = concatenate([mlp_descs2, mlp_kpts2])
    #mlp_out = TimeDistributed(Dense(2048, activation='tanh'))(mlp_concat)
    mlp_out = Dense(1024)(mlp_concat)

    #input_imu = Input(batch_shape=(batch_size, 2, 9),name="imu")

    #input_lstm = concatenate([mlp_out, input_imu])
    lstm1 = LSTM(1024,return_sequences=True)(mlp_out)
    lstm2 = LSTM(1024)(lstm1)
    #output1 = Dense(256)(lstm2)
    output = Dense(6)(lstm2)

    #return Model(inputs=[input_descs, input_kpts, input_imu], outputs=output)
    return Model(inputs=[input_descs, input_kpts], outputs=output)


def mlp(descs_input_shape, kpts_input_shape):

    batch_size = descs_input_shape[0]
    input_descs = Input(batch_shape=(descs_input_shape), name="descriptors")
    mlp_descs = Dense(4000)(input_descs)
    mlp_descs1 = Dense(4000)(mlp_descs)
    mlp_descs2 = Dense(1000)(mlp_descs1)

    #batch_norm1 = BatchNormalization()(mlp_descs)

    input_kpts = Input(batch_shape=(kpts_input_shape), name="kpts")
    mlp_kpts = Dense(2600*2)(input_kpts)
    mlp_kpts2 = Dense(260*2)(mlp_kpts)
    #batch_norm2 = BatchNormalization()(mlp_kpts)

    mlp_concat = concatenate([mlp_descs2, mlp_kpts2])
    mlp_out = Dense(1024)(mlp_concat)
    #output1 = Dense(256)(mlp_out)
    output = Dense(6)(mlp_out)

    return Model(inputs=[input_descs, input_kpts], outputs=output)
