import os
import glob
from dataset import Dataset
import numpy as np
from model import mlp_stacked_lstm, mlp
import keras
from utils import read_csv, read_imu
from convert import quaternion_to_euler, euler_to_quaternion
from loss import *


def get_iterator(data_path, labels_path, cycle_every, batch_size, max_kpts=100, max_skip=1, evaluation=False):
    random_seed = 42
    labels_files = glob.glob(labels_path+"/*.csv")
    imu_files =  glob.glob(labels_path+"/imu/*.csv")
    labels = []

    for label in labels_files:
        print(label)
        csv=read_csv(label)
        labels.append(csv)

    imu = read_imu(imu_files[0])

    for i in range(1, len(imu_files)):
        imu = imu.append(read_imu(imu_files[i]))

    rand = np.random.RandomState(random_seed)
    return Dataset(labels, data_path, imu, batch_size=batch_size, max_kpts=max_kpts,
                   max_skip= max_skip, rand=rand, cycle_every=cycle_every, evaluation=evaluation)

def train(model, train_xys, val_xys, steps_per_epoch, epochs, val_max_n_its, batch_size, logs_dir_path, ckpts_dir_path):

    callbacks = [
        keras.callbacks.TensorBoard(log_dir=logs_dir_path),
        keras.callbacks.ModelCheckpoint(
            os.path.join(ckpts_dir_path, 'model-{epoch:02d}-{val_loss:.2f}.h5'),
            monitor='val_loss',
            save_best_only=False)
        ]
    print('fitting...')
    history = model.fit_generator(
        generator=train_xys.iterate(),
        steps_per_epoch=steps_per_epoch,
        epochs=epochs,
        verbose=1,
        validation_data=val_xys.iterate(),
        validation_steps=val_max_n_its,
        use_multiprocessing=True,
        callbacks=callbacks)

    return history, model

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("train_path", default=None, help="Path to training data")
    parser.add_argument("valid_path", default=None, help="Path to validation data")
    parser.add_argument("labels_train", default=None, help="Path to CSV labels for training")
    parser.add_argument("labels_valid", default=None, help="Path to CSV labels for validation")
    parser.add_argument("--units", default=100, type=int)
    parser.add_argument("--steps_per_epoch", default=2000, type=int)
    parser.add_argument("--epochs", default=100, type=int)
    parser.add_argument("--batch_size", default=32, type=int)
    parser.add_argument("--logs_dir", default="logs/")
    parser.add_argument("--checkpoints_dir", default="checkpoints/")
    parser.add_argument("--load_model", default=None)
    parser.add_argument("--n_frames_train")
    parser.add_argument("--n_frames_val")
    args = parser.parse_args()

    units = args.units
    #steps_per_epoch = args.steps_per_epoch
    #val_max_n_its = steps_per_epoch//2

    epochs = args.epochs
    batch_size = args.batch_size
    logs_dir_path = args.logs_dir
    checkpoints_dir_path = args.checkpoints_dir
    steps_per_epoch = int(args.n_frames_train)//batch_size
    val_max_n_its = int(args.n_frames_val)//batch_size

    print("steps_per_epoch ",steps_per_epoch)
    print("val_max_n_its ",val_max_n_its)

    if(not os.path.exists(logs_dir_path)):
        os.mkdir(logs_dir_path)

    if(not os.path.exists(checkpoints_dir_path)):
        os.mkdir(checkpoints_dir_path)

    train_iterator = get_iterator(args.train_path, args.labels_train, None, batch_size, max_kpts=100, max_skip=3)
    valid_iterator = get_iterator(args.valid_path, args.labels_valid, val_max_n_its, batch_size, max_kpts=100, max_skip=1)

    #for x, y in valid_iterator.iterate():
    #    print(y)

    #model = mlp_stacked_lstm((batch_size, 2, 100*128), (batch_size, 2, 100*13))
    model = mlp_stacked_lstm((batch_size, 1, 2*100*128), (batch_size, 1, 2*100*13))
    #model = mlp((batch_size, 1, 2*100*128), (batch_size, 1, 2*100*13))
    if args.load_model:
        model.load_weights(args.load_model)
        print("Loaded weights from ", args.load_model)

    model.summary()
    keras.utils.plot_model(model, to_file='model.png')

    #opt = keras.optimizers.Adam(lr=0.0001)
    opt = keras.optimizers.Adagrad(lr=0.001)
    model.compile(loss = custom_loss(alpha=1000, beta=1000, batch_size=batch_size),
                  optimizer=opt,
                  metrics=['mae', 'mse', get_pred_x(batch_size), get_true_x(batch_size),
                    get_pred_y(batch_size), get_true_y(batch_size), get_pred_z(batch_size),
                    get_true_z(batch_size), get_pred_roll(batch_size), get_true_roll(batch_size),
                    get_pred_pitch(batch_size), get_true_pitch(batch_size), get_pred_yaw(batch_size),
                    get_true_yaw(batch_size)])

    history, model = train(model, train_iterator, valid_iterator, steps_per_epoch, epochs,
            batch_size, val_max_n_its, logs_dir_path, checkpoints_dir_path)

    test_iterator = get_iterator(args.valid_path, args.labels_valid, val_max_n_its,
        batch_size=1, max_kpts=100, max_skip=1, evaluation=True)

    preds=[]
    last_pose = np.array([0,0,0,0,0,0],dtype='float64')
    count=1
    print("TESTING")
    for x, y, ts in test_iterator.iterate():
        if(count==val_max_n_its):
            break
        count+=1
        pred = model.predict(x)
        print("pred ", pred)
        print("true ", y)
        result = model.evaluate(x=x, y=y, use_multiprocessing=True)
        print("eval ", result)

        last_pose += pred[0][0]/1000
        #last_pose = pred[0]
        preds.append([ts, last_pose.copy()])

    print(preds)
    print("Saving")
    pose = np.array([0,0,0,0,0,0],dtype='float64')
    for i in range(len(preds)):
        pose=preds[i][1]
        with open(checkpoints_dir_path+'/prediction.csv', 'a') as file:
            #file.write(preds[i][0][0][2:]) #kitti
            file.write("{:.9f}".format(float(preds[i][0][0])/1e9)) #euroc
            pose = np.hstack((pose[:3], euler_to_quaternion(pose[3:])))
            for p in pose:
                file.write(" "+str(p))
            file.write("\n")
        file.close()
