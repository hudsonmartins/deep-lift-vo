import os
import numpy as np
from utils import loadh5, readh5, min_max_norm
from convert import quaternion_to_euler
from loss import custom_loss

def limit_shape(arr, max_kpts):
    if(arr.shape[0] < max_kpts):
        #fill with zeros
        return np.pad(arr, [(0, max_kpts-arr.shape[0]), (0,0)], 'constant')
    elif(arr.shape[0] >= max_kpts):
        #cut array in the max number of rows
        return arr[:max_kpts, :]

def to_network_input(descs, max_kpts):
    desc = min_max_norm(limit_shape(descs['descriptors'], max_kpts).flatten())
    kpt = min_max_norm(limit_shape(descs['keypoints'], max_kpts).flatten())

    #desc = limit_shape(descs['descriptors'], max_kpts).flatten()
    #kpt = limit_shape(descs['keypoints'], max_kpts).flatten()
    return desc, kpt

class Dataset():
    def __init__(self, ground_truth, data_path, batch_size, max_kpts, max_skip, rand, evaluation=False, cycle_every=None):
        '''
            ground_truth: list with csv readings (ts, x, y, z, qw, qx, qy, qz)
        '''
        print("Started dataset")
        #self._ys = ground_truth #list of lists
        #Converting the origin to (0,0,0,0,0,0)
        self._ys = []

        for sequence in ground_truth:
            new_sequence = []
            for row in sequence:
                timestamp = [row[0]]
                new_row_angle = quaternion_to_euler(row[4:])
                new_row = row[1:4] + new_row_angle
                new_sequence.append(timestamp + new_row)
            self._ys.append(new_sequence)

        #self._ys = ground_truth
        self._batch_size = batch_size
        self._rand = rand
        self._rand_state = self._rand.get_state()
        self._data_path = data_path
        self._max_kpts = max_kpts
        self._max_skip = max_skip+1

        self._cycle_every = cycle_every
        self._n_iterations = 0
        self._sequences = list(range(len(self._ys)))
        self._curr_sequence = 0
        self._last_frame = 0
        self._curr_pose = [0,0,0,0,0,0]
        self.evaluation=evaluation

    def reset(self):
        self._rand.set_state(self._rand_state)
        self._n_iterations = 0

    def get_batch(self):
        if self._cycle_every is not None and self._n_iterations > 0 \
                and self._n_iterations % self._cycle_every == 0:
            self.reset()

        descs, kpts, ys, ts = [], [], [], []

        for _ in range(self._batch_size):

            #First image
            if(self._last_frame >= len(self._ys[self._curr_sequence]) - self._max_skip):
                self._curr_sequence += 1
                self._last_frame = 0
                self._curr_pose = [0,0,0,0,0,0]
                if(self._curr_sequence == len(self._sequences)):
                    self._curr_sequence = 0

            sequence = self._curr_sequence
            #print("Sequence: ", sequence)
            x1 = None
            while not x1:
                if(not self.evaluation):
                    index = self._rand.choice(list(range(len(self._ys[sequence]))))
                else:
                    index = self._last_frame
                #print(index)
                timestamp = self._ys[sequence][index][0]
                x1 = loadh5(self._data_path+timestamp+'.png_desc.h5')

            #print("t ",timestamp)
            #Second image
            #Choose a frame between t+1 and t+max_skip
            x2 = None
            while not x2:
                pair_index = index + self._rand.choice(list(range(1, self._max_skip)))
                if(pair_index >= len(self._ys[sequence])):
                    pair_index = index
                #print(pair_index)
                timestamp = self._ys[sequence][pair_index][0]
                x2 = loadh5(self._data_path+timestamp+'.png_desc.h5')
            #print("t+1 ",timestamp)
            self._last_frame = pair_index
            ts.append(timestamp)
            #Convert to input
            #x = np.array([to_network_input(x1, self._max_kpts), to_network_input(x2, self._max_kpts)])
            desc1, kpt1 = to_network_input(x1, self._max_kpts)
            desc2, kpt2 = to_network_input(x2, self._max_kpts)
            desc = [np.hstack((desc1,desc2))]
            kpt = [np.hstack((kpt1, kpt2))]

            delta_pose = np.asarray(self._ys[sequence][pair_index][1:]) - np.asarray(self._ys[sequence][index][1:])
            y = delta_pose

            descs.append(np.asarray(desc))
            kpts.append(np.asarray(kpt))
            ys.append(np.asarray([y]))

        descs, kpts, ys = np.stack(descs, axis=0), np.stack(kpts, axis=0), np.stack(ys, axis=0)
        self._n_iterations += 1
        if(not self.evaluation):
            return ([descs, kpts], ys)
        return ([descs, kpts], ys, ts)


    def iterate(self):
        while True:
            yield self.get_batch()
