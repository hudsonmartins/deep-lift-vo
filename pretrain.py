import os
import tensorflow as tf
import glob
from dataset_pretrain import Dataset
import numpy as np
from model import mlp
import keras
import keras.backend as K
from utils import read_csv
from loss import custom_loss

def get_iterator(data_path, labels_path, cycle_every, batch_size, max_kpts, max_skip, evaluation=False):
    random_seed = 42
    labels_files = glob.glob(labels_path+"/*.csv")

    labels = []
    for label in labels_files:
        print(label)
        csv=read_csv(label)
        #print(csv)
        labels.append(csv)

    rand = np.random.RandomState(random_seed)
    return Dataset(labels, data_path, batch_size=batch_size, max_kpts=max_kpts,
                   max_skip= max_skip, rand=rand, cycle_every=cycle_every, evaluation=evaluation)


def train(model, train_xys, val_xys, steps_per_epoch, epochs, val_max_n_its, batch_size, logs_dir_path, ckpts_dir_path):

    callbacks = [
        keras.callbacks.TensorBoard(log_dir=logs_dir_path),
        keras.callbacks.ModelCheckpoint(
            os.path.join(ckpts_dir_path, 'model-{epoch:02d}-{val_loss:.2f}.h5'),
            monitor='val_loss',
            save_best_only=True)
        ]
    print('fitting...')
    history = model.fit_generator(
        generator=train_xys.iterate(),
        steps_per_epoch=steps_per_epoch,
        epochs=epochs,
        verbose=1,
        validation_data=val_xys.iterate(),
        validation_steps=val_max_n_its,
        use_multiprocessing=True,
        callbacks=callbacks)

    return history

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("train_path", default=None, help="Path to training data")
    parser.add_argument("valid_path", default=None, help="Path to validation data")
    parser.add_argument("labels_train", default=None, help="Path to CSV labels for training")
    parser.add_argument("labels_valid", default=None, help="Path to CSV labels for validation")
    parser.add_argument("--units", default=100, type=int)
    parser.add_argument("--steps_per_epoch", default=2000, type=int)
    parser.add_argument("--epochs", default=100, type=int)
    parser.add_argument("--batch_size", default=32, type=int)
    parser.add_argument("--logs_dir", default="logs/")
    parser.add_argument("--checkpoints_dir", default="checkpoints/")
    parser.add_argument("--load_model", default=None)
    parser.add_argument("--n_frames_train")
    parser.add_argument("--n_frames_val")
    args = parser.parse_args()

    units = args.units
    #steps_per_epoch = args.steps_per_epoch
    #val_max_n_its = steps_per_epoch//2

    epochs = args.epochs
    batch_size = args.batch_size
    logs_dir_path = args.logs_dir
    checkpoints_dir_path = args.checkpoints_dir
    steps_per_epoch = int(args.n_frames_train)//batch_size
    val_max_n_its = int(args.n_frames_val)//batch_size

    print("steps_per_epoch ",steps_per_epoch)
    print("val_max_n_its ",val_max_n_its)

    if(not os.path.exists(logs_dir_path)):
        os.mkdir(logs_dir_path)

    if(not os.path.exists(checkpoints_dir_path)):
        os.mkdir(checkpoints_dir_path)

    train_iterator = get_iterator(args.train_path, args.labels_train, None, batch_size, max_kpts=300, max_skip=5)
    valid_iterator = get_iterator(args.valid_path, args.labels_valid, val_max_n_its, batch_size, max_kpts=300, max_skip=1)
    #for x,y in train_iterator.iterate():
    #    print(y)

    model = mlp((batch_size, 1, 2*300*128), (batch_size, 1, 2*300*13))
    if args.load_model:
        model.load_weights(args.load_model)
        print("Loaded weights from ", args.load_model)

    model.summary()
    keras.utils.plot_model(model, to_file='model.png')

    #sgd=keras.optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9)
    adam = keras.optimizers.Adam(lr=0.001)
    model.compile(loss = custom_loss(beta=10, batch_size=batch_size),
                  optimizer=adam,
                  metrics=['mae', 'mse', custom_loss(beta=10, batch_size=batch_size)])

    train(model, train_iterator, valid_iterator, steps_per_epoch, epochs,
          batch_size, val_max_n_its, logs_dir_path, checkpoints_dir_path)
