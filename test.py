import os
from model import mlp_stacked_lstm, mlp
import glob
import dataset
import numpy as np
from utils import read_csv,read_imu
from convert import quaternion_to_euler, euler_to_quaternion
from loss import custom_loss
from dataset import Dataset
import keras

def get_iterator(data_path, labels_path, cycle_every, batch_size, max_kpts, max_skip, evaluation=False):
    random_seed = 42
    labels_files = glob.glob(labels_path+"/*.csv")
    imu_files =  glob.glob(labels_path+"/imu/*.csv")
    labels = []

    for label in labels_files:
        print(label)
        csv=read_csv(label)
        labels.append(csv)

    imu = read_imu(imu_files[0])

    for i in range(1, len(imu_files)):
        imu = imu.append(read_imu(imu_files[i]))

    rand = np.random.RandomState(random_seed)
    return Dataset(labels, data_path, imu, batch_size=batch_size, max_kpts=max_kpts,
                   max_skip= max_skip, rand=rand, cycle_every=cycle_every, evaluation=True)

def test(test_dir, model_path, output_path, max_its, ground_truth):

    batch_size = 1
    #model = mlp_stacked_lstm((batch_size, 2, 100*128), (batch_size, 2, 100*13))
    model = mlp_stacked_lstm((batch_size, 1, 2*100*128), (batch_size, 1, 2*100*13))
    #model = mlp((batch_size, 1, 2*100*128), (batch_size, 1, 2*100*13))
    model.load_weights(model_path)
    print("Loaded weights from ", model_path)
    model.summary()
    #opt = keras.optimizers.Adam(lr=0.0001)
    opt = keras.optimizers.Adagrad(lr=0.001)
    model.compile(loss = custom_loss(alpha=10,beta=1000, batch_size=batch_size),
                  optimizer=opt,
                  metrics=['mae', 'mse', custom_loss(alpha=1,beta=100, batch_size=batch_size)])
    test_iterator = get_iterator(test_dir, ground_truth, max_its, batch_size, max_kpts=100, max_skip=1, evaluation=True)

    preds=[]
    descs_path = sorted(glob.glob(test_dir+"/*.h5"))
    last_pose = np.array([0,0,0,0,0,0],dtype='float64')
    count=1

    for x, y, ts in test_iterator.iterate():
        if(count==max_its):
            break
        count+=1
        #p = [last_pose,last_pose]
        #x = np.hstack((x, [p]))
        pred = model.predict(x)
        print("pred ", pred)
        print("true ", y)
        result = model.evaluate(x=x, y=y, use_multiprocessing=True)
        print("eval ", result)
        last_pose += pred[0][0]
        #last_pose = pred[0]
        preds.append([ts, last_pose.copy()])

    print(preds)
    print("Saving")
    pose = np.array([0,0,0,0,0,0],dtype='float64')
    for i in range(len(preds)):
        pose=preds[i][1]
        with open(output_path+'/prediction.csv', 'a') as file:
            #file.write(preds[i][0][0][2:]) #kitti
            file.write("{:.9f}".format(float(preds[i][0][0])/1e9)) #euroc
            pose = np.hstack((pose[:3], euler_to_quaternion(pose[3:])))
            for p in pose:
                file.write(" "+str(p))
            file.write("\n")
        file.close()

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("test_path", default=None, help="Path to test data")
    parser.add_argument("model_path", default=None)
    parser.add_argument("--output_path", default="predictions")
    parser.add_argument("--ground_truth")
    parser.add_argument("--max_its", default=1100)

    args = parser.parse_args()
    if not os.path.exists(args.output_path):
        os.mkdir(args.output_path)

    test(args.test_path, args.model_path, args.output_path, int(args.max_its), args.ground_truth)
