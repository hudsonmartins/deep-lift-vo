import keras.backend as K
import numpy as np

def custom_loss(alpha, beta, batch_size):
    def weighted_mse(y_true, y_pred):
        """Custom loss function for jointly learning
        position and orientation.
        """
        squared_diff = K.square(y_pred - y_true)
        y_shape = K.int_shape(squared_diff)
        y_shape = (batch_size,1,6)
        #y_shape = (batch_size,) + y_shape[1:]

        weights = np.ones(y_shape)
        weights[...,:3] = alpha
        weights[..., 3:] = beta
        weights = K.variable(weights)

        # element-wise multiplication
        squared_diff_weighted = squared_diff * weights
        loss = K.mean(squared_diff_weighted, axis=-1)

        return loss
    return weighted_mse

def get_pred_x(batch_size):
    def pred_x(y_true, y_pred):
        return y_pred[...,0]
    return pred_x

def get_true_x(batch_size):
    def true_x(y_true, y_pred):
        return y_true[...,0]
    return true_x

def get_pred_y(batch_size):
    def pred_y(y_true, y_pred):
        return y_pred[...,1]
    return pred_y

def get_true_y(batch_size):
    def true_y(y_true, y_pred):
        return y_true[...,1]
    return true_y

def get_pred_z(batch_size):
    def pred_z(y_true, y_pred):
        return y_pred[...,2]
    return pred_z

def get_true_z(batch_size):
    def true_z(y_true, y_pred):
        return y_true[...,2]
    return true_z

def get_pred_roll(batch_size):
    def pred_roll(y_true, y_pred):
        return y_pred[...,3]
    return pred_roll

def get_true_roll(batch_size):
    def true_roll(y_true, y_pred):
        return y_true[...,3]
    return true_roll

def get_pred_pitch(batch_size):
    def pred_pitch(y_true, y_pred):
        return y_pred[...,4]
    return pred_pitch

def get_true_pitch(batch_size):
    def true_pitch(y_true, y_pred):
        return y_true[...,4]
    return true_pitch

def get_pred_yaw(batch_size):
    def pred_yaw(y_true, y_pred):
        return y_pred[...,5]
    return pred_yaw

def get_true_yaw(batch_size):
    def true_yaw(y_true, y_pred):
        return y_true[...,5]
    return true_yaw
